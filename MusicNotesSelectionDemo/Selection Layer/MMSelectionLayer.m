//
//  MMSelectionLayer.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMSelectionLayer.h"

@interface MMSelectionLayer()

@property (nonatomic, assign, readwrite) CGRect rect;
@property (nonatomic, assign, readwrite) MMSelectionLayerType selectionType;
@property (nonatomic, strong, readwrite) UIColor* color;
@property (nonatomic, assign, readwrite) BOOL isBeginning;

@end

@implementation MMSelectionLayer

#pragma mark - Initializers

- (instancetype)init {
    self = [super init];
    if (self) {
        self.lineWidth = 1.0;
        self.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.4].CGColor;
        
        _arrowSize = 15.0f;
    }
    return self;
}

#pragma mark - Public Methods

- (void)highlightRect:(CGRect)rect selectionType:(MMSelectionLayerType)selectionType color:(UIColor *)color isBeginning:(BOOL)isBeginning {
    self.rect = rect;
    self.selectionType = selectionType;
    self.color = color;
    self.isBeginning = isBeginning;
    
    self.fillColor = color.CGColor;
    
    UIBezierPath* bezierPath = nil;
    switch (selectionType) {
        case MMSelectionLayerTypeBullet:
            bezierPath = [self _pathForBullet:rect isBeginning:isBeginning];
            break;
        case MMSelectionLayerTypeRectangle:
            bezierPath = [self _pathForRectangle:rect];
            break;
        case MMSelectionLayerTypeRoundedRectangle:
            bezierPath = [self _pathForRoundedRectangle:rect];
            break;
        case MMSelectionLayerTypeDiamond:
            bezierPath = [self _pathForDiamond:rect];
            break;
        case MMSelectionLayerTypeLowerTriangle:
            bezierPath = [self _pathForLowerTriangle:rect];
            break;
        case MMSelectionLayerTypeUpperTriangle:
            bezierPath = [self _pathForUpperTriangle:rect];
            break;
    }
    
    self.path = bezierPath.CGPath;
}

#pragma mark - Private Methods

- (UIBezierPath *)_pathForBullet:(CGRect)rect isBeginning:(BOOL)isBeginning {
    UIBezierPath* bezierPath = nil;
    CGSize cornerRadius = CGSizeMake(6.0, 6.0);
    if (isBeginning) {
        bezierPath = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:cornerRadius];
    } else {
        bezierPath = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:cornerRadius];
    }
    UIBezierPath* lineBezierPath = [UIBezierPath bezierPath];
    if (isBeginning) {
        [lineBezierPath moveToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width + self.arrowSize, rect.origin.y + rect.size.height/2.0)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)];
        [lineBezierPath closePath];
    } else {
        [lineBezierPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x - self.arrowSize, rect.origin.y + rect.size.height/2.0)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
        [lineBezierPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y)];
        [lineBezierPath closePath];
    }
    [bezierPath appendPath:lineBezierPath];
    return bezierPath;
}

- (UIBezierPath *)_pathForRectangle:(CGRect)rect {
    UIBezierPath* bezierPath = [UIBezierPath bezierPathWithRect:rect];
    return bezierPath;
}

- (UIBezierPath *)_pathForRoundedRectangle:(CGRect)rect {
    UIBezierPath* bezierPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:3.0];
    return bezierPath;
}

- (UIBezierPath *)_pathForDiamond:(CGRect)rect {
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height/2.0)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width/2.0, rect.origin.y + rect.size.height)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height/2.0)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width/2.0, rect.origin.y)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height/2.0)];
    [bezierPath closePath];
    return bezierPath;
}

- (UIBezierPath *)_pathForUpperTriangle:(CGRect)rect {
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width/2.0, rect.origin.y + rect.size.height)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y)];
    [bezierPath closePath];
    return bezierPath;
}

- (UIBezierPath *)_pathForLowerTriangle:(CGRect)rect {
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width/2.0, rect.origin.y)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height)];
    [bezierPath addLineToPoint:CGPointMake(rect.origin.x, rect.origin.y + rect.size.height)];
    [bezierPath closePath];
    return bezierPath;
}

@end
