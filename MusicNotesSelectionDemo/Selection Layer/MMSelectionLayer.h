//
//  MMSelectionLayer.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MMSelectionLayerType) {
    MMSelectionLayerTypeRectangle,
    MMSelectionLayerTypeRoundedRectangle,
    MMSelectionLayerTypeDiamond,
    MMSelectionLayerTypeUpperTriangle,
    MMSelectionLayerTypeLowerTriangle,
    MMSelectionLayerTypeBullet,
};

@interface MMSelectionLayer : CAShapeLayer

- (void)highlightRect:(CGRect)rect selectionType:(MMSelectionLayerType)selectionType color:(UIColor *)color isBeginning:(BOOL)isBeginning;

@property (nonatomic, assign, readonly) CGRect rect;
@property (nonatomic, assign, readonly) MMSelectionLayerType selectionType;
@property (nonatomic, strong, readonly) UIColor* color;
@property (nonatomic, assign, readonly) BOOL isBeginning;


@property (nonatomic, assign) CGFloat arrowSize;

@end
