//
//  main.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
