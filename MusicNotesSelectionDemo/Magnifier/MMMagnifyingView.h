//
//  MMMagnifyingView.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMMagnifyingLoopView;

@interface MMMagnifyingView : UIView

@property (nonatomic, strong) MMMagnifyingLoopView* magnifyingLoopView;

@end

@interface MMMagnifyingLoopView : UIView

@property (nonatomic, weak) UIView* viewToMagnify;
@property (nonatomic, assign) CGFloat verticalOffset;
@property (nonatomic, assign) CGPoint zoomingLocation;
@property (nonatomic, assign) CGFloat zoomingScaleFactor;

@end
