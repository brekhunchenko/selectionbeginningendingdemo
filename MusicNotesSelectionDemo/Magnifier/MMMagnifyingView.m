//
//  MMMagnifyingView.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/20/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMMagnifyingView.h"

@interface MMMagnifyingView()

@property (nonatomic, strong) NSTimer* timer;

@end

@implementation MMMagnifyingView

#pragma mark - Initializers

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self _setupMagnifyingLoopView];
}

#pragma mark - Setup

- (void)_setupMagnifyingLoopView {
    _magnifyingLoopView = [[MMMagnifyingLoopView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 120.0f, 75.0f)];
    _magnifyingLoopView.viewToMagnify = self;
}

#pragma mark - UIResponder

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(_addMagnifyingLoopView) userInfo:nil repeats:NO];
    UITouch* touch = [touches anyObject];
    if (touch) {
        CGPoint location = [touch locationInView:self];
        [self _updateMagnifyingLoopViewAtLocation:location];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    UITouch* touch = [touches anyObject];
    if (touch) {
        CGPoint location = [touch locationInView:self];
        [self _updateMagnifyingLoopViewAtLocation:location];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self _removeMagnifyingLoopView];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    [self _removeMagnifyingLoopView];
}

#pragma mark - Private Methods

- (void)_addMagnifyingLoopView {
    [self addSubview:_magnifyingLoopView];
}

- (void)_removeMagnifyingLoopView {
    [_timer invalidate];
    _timer = nil;
    
    [_magnifyingLoopView removeFromSuperview];
}

- (void)_updateMagnifyingLoopViewAtLocation:(CGPoint)location {
    _magnifyingLoopView.zoomingLocation = location;
}

@end

@implementation MMMagnifyingLoopView

#pragma mark - Initializers

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.layer.borderWidth = 3.0f;
        self.layer.cornerRadius = 5.0f;
        
        self.verticalOffset = frame.size.height;
        self.zoomingScaleFactor = 2.0f;
    }
    return self;
}

#pragma mark - Custom Setters & Getters

- (void)setVerticalOffset:(CGFloat)verticalOffset {
    if (verticalOffset < self.bounds.size.height) {
        NSLog(@"Error. Magnifier loop offset can't be less than its height.");
    }
    
    _verticalOffset = MAX(self.bounds.size.height, verticalOffset);
}

- (void)setZoomingLocation:(CGPoint)zoomingLocation {
    _zoomingLocation = zoomingLocation;
    
    self.center = CGPointMake(_zoomingLocation.x, _zoomingLocation.y - _verticalOffset);
    [self setNeedsDisplay];
}

#pragma mark - Overridden

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, self.bounds.size.width/2.0f, self.bounds.size.height/2.0f);
    CGContextScaleCTM(context, self.zoomingScaleFactor, self.zoomingScaleFactor);
    CGContextTranslateCTM(context, -self.zoomingLocation.x, -self.zoomingLocation.y);
    [self.viewToMagnify.layer renderInContext:context];
}

@end
