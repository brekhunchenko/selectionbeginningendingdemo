//
//  MMLongPressGestureRecognizer.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/19/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMLongPressGestureRecognizer;

@protocol MMLongPressGestureRecognizerDelegate < UIGestureRecognizerDelegate >

@optional
- (void)longPressGestureRecognizer:(MMLongPressGestureRecognizer *_Nonnull)gestureRecognizer didMakePauseAtLocation:(CGPoint)location;

@end

@interface MMLongPressGestureRecognizer : UILongPressGestureRecognizer < UIGestureRecognizerDelegate > {
    @private
        id<UIGestureRecognizerDelegate, MMLongPressGestureRecognizerDelegate> __weak _myDelegate;
}

@property (nullable, nonatomic, weak) id<MMLongPressGestureRecognizerDelegate> delegate;

@end
