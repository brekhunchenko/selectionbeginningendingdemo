//
//  MMLongPressGestureRecognizer.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/19/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMLongPressGestureRecognizer.h"
#import <objc/runtime.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

static BOOL protocol_containsSelector(Protocol *protocol, SEL selector) {
    return protocol_getMethodDescription(protocol, selector, YES, YES).name != NULL || protocol_getMethodDescription(protocol, selector, NO, YES).name != NULL;
}

@interface MMLongPressGestureRecognizer()

@property (nonatomic, strong) NSMutableArray<NSValue *>* previousLocations;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, assign) CGPoint previousLocation;
@property (nonatomic, assign) CGPoint previousLongStayLocation;

@end

@implementation MMLongPressGestureRecognizer

#pragma mark - Initializers

- (instancetype)initWithTarget:(id)target action:(SEL)action {
    self = [super initWithTarget:target action:action];
    if (self) {
        _previousLocations = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Custom Setters & Getters

- (id<UIGestureRecognizerDelegate>)delegate {
    return [super delegate];
}

- (void)setDelegate:(id<MMLongPressGestureRecognizerDelegate>)delegate {
    _myDelegate = delegate;
    if (delegate) {
        [super setDelegate:self];
    } else{
        [super setDelegate:nil];
    }
}

#pragma mark - NSObject

- (BOOL)respondsToSelector:(SEL)aSelector {
    if (protocol_containsSelector(@protocol(UIGestureRecognizerDelegate), aSelector)) {
        return [super respondsToSelector:aSelector] || [_myDelegate respondsToSelector:aSelector];
    }
    
    return [super respondsToSelector:aSelector];
}

- (id)forwardingTargetForSelector:(SEL)aSelector {
    if (protocol_containsSelector(@protocol(UIGestureRecognizerDelegate), aSelector)) {
        return _myDelegate;
    }
    
    return [super forwardingTargetForSelector:aSelector];
}

#pragma mark - UIGestureRecognizer

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    if (touches.count > 1) {
        self.state = UIGestureRecognizerStateFailed;
        return;
    }
    
    _previousLocation = [touches.anyObject locationInView:self.view];
    
    [self _beginTimerUpdates];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    
    _previousLocation = [touches.anyObject locationInView:self.view];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self _endTimerUpdates];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    self.state = UIGestureRecognizerStateCancelled;
    
    [self _endTimerUpdates];
}

#pragma mark - Private Methods

- (void)_appendGestureLocation:(CGPoint)location {
    if (_previousLocations.count == [self _requiredNumberOfPreviousLocations]) {
        [_previousLocations removeObjectAtIndex:0];
    }
    
    [_previousLocations addObject:[NSValue valueWithCGPoint:location]];
}

- (BOOL)_isUserStaysOnSameLocation {
    if (_previousLocations.count < [self _requiredNumberOfPreviousLocations]) {
        return NO;
    }
    
    BOOL isUserStaysOnSameLocation = YES;
    for (int i = 0; i < _previousLocations.count - 1; i++) {
        int aPointIndex = i;
        int bPointIndex = (int)(_previousLocations.count - i - 1);
        if (aPointIndex >= bPointIndex) {
            break;
        }
        NSValue* cgpointValue_0 = _previousLocations[aPointIndex];
        NSValue* cgpointValue_1 = _previousLocations[bPointIndex];
        if ([self _pointFuzzyEqual:cgpointValue_0.CGPointValue
                           toPoint:cgpointValue_1.CGPointValue
                          variance:self.allowableMovement] == NO) {
            isUserStaysOnSameLocation = NO;
            break;
        }
    }
    
    return isUserStaysOnSameLocation;
}

- (void)_timerUpdate:(NSTimer *)timer {
    CGPoint location = _previousLocation;
    [self _appendGestureLocation:location];
    
    if ([self _isUserStaysOnSameLocation]) {
        if ([self _pointFuzzyEqual:_previousLongStayLocation
                           toPoint:location
                          variance:self.allowableMovement] == NO) {
            _previousLongStayLocation = location;
            
            if ([_myDelegate respondsToSelector:@selector(longPressGestureRecognizer:didMakePauseAtLocation:)]) {
                [_myDelegate longPressGestureRecognizer:self didMakePauseAtLocation:location];
            }
        }
    }
}

- (void)_beginTimerUpdates {
    [_timer invalidate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0/60.0 target:self selector:@selector(_timerUpdate:) userInfo:nil repeats:YES];
}

- (void)_endTimerUpdates {
    [_timer invalidate];
    _timer = nil;
}

- (BOOL)_pointFuzzyEqual:(CGPoint)a toPoint:(CGPoint)b variance:(CGFloat)var {
    if(a.x - var <= b.x && b.x <= a.x + var)
        if(a.y - var <= b.y && b.y <= a.y + var)
            return YES;
    return NO;
}

- (NSInteger)_requiredNumberOfPreviousLocations {
    return 20;
}

@end
