//
//  CDCurve+CoreDataProperties.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 4/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//
//

#import "CDCurve+CoreDataProperties.h"

@implementation CDCurve (CoreDataProperties)

+ (NSFetchRequest<CDCurve *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDCurve"];
}

@dynamic color;
@dynamic lineWidth;
@dynamic page;
@dynamic path;
@dynamic username;
@dynamic arrangementID;
@dynamic scoreEdition;

@end
