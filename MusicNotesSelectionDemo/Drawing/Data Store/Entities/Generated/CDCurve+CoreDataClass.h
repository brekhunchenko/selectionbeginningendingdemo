//
//  CDCurve+CoreDataClass.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 4/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface CDCurve : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CDCurve+CoreDataProperties.h"
