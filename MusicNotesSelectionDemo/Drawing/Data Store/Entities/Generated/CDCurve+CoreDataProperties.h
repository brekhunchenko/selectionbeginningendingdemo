//
//  CDCurve+CoreDataProperties.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 4/21/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//
//

#import "CDCurve+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDCurve (CoreDataProperties)

+ (NSFetchRequest<CDCurve *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *color;
@property (nonatomic) float lineWidth;
@property (nullable, nonatomic, copy) NSString *page;
@property (nullable, nonatomic, retain) NSObject *path;
@property (nullable, nonatomic, copy) NSString *username;
@property (nullable, nonatomic, copy) NSString *arrangementID;
@property (nullable, nonatomic, copy) NSString *scoreEdition;

@end

NS_ASSUME_NONNULL_END
