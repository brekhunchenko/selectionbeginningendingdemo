//
//  CDCurve+CoreDataExtensions.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "CDCurve+CoreDataClass.h"

@interface CDCurve (CoreDataExtensions)

+ (CDCurve *)findOrCreateCurveForUsername:(NSString *)username
                                     page:(NSString *)page
                            arrangementID:(NSString *)arrangementID
                             scoreEdition:(NSString *)scoreEdition;
+ (CDCurve *)findCurveForUsername:(NSString *)username
                             page:(NSString *)page
                    arrangementID:(NSString *)arrangementID
                     scoreEdition:(NSString *)scoreEdition;

@end
