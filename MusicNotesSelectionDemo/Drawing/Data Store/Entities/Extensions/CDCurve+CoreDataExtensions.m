//
//  CDCurve+CoreDataExtensions.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "CDCurve+CoreDataExtensions.h"
#import "MMDrawingDataStore.h"

@implementation CDCurve (CoreDataExtensions)

+ (CDCurve *)findOrCreateCurveForUsername:(NSString *)username
                                     page:(NSString *)page
                            arrangementID:(NSString *)arrangementID
                             scoreEdition:(NSString *)scoreEdition {
    CDCurve* curve = [self findCurveForUsername:username page:page arrangementID:arrangementID scoreEdition:scoreEdition];
    if (curve != nil) {
        return curve;
    } else {
        CDCurve* curve = [[CDCurve alloc] initWithContext:[MMDrawingDataStore sharedInstance].persistentContainer.viewContext];
        return curve;
    }
}

+ (CDCurve *)findCurveForUsername:(NSString *)username
                             page:(NSString *)page
                    arrangementID:(NSString *)arrangementID
                     scoreEdition:(NSString *)scoreEdition {
    NSFetchRequest* fetchRequest = CDCurve.fetchRequest;
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"username == %@ AND page == %@ AND arrangementID == %@ AND scoreEdition == %@", username, page, arrangementID, scoreEdition];
    NSArray<CDCurve *>* results = [[MMDrawingDataStore sharedInstance].persistentContainer.viewContext executeFetchRequest:fetchRequest error:nil];
    return results.firstObject;
}

@end
