//
//  MMDrawingDataStore.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMDrawingDataStore.h"

@interface MMDrawingDataStore()

@end

@implementation MMDrawingDataStore

@synthesize persistentContainer = _persistentContainer;

#pragma mark - Initializers

+ (instancetype)sharedInstance {
    static MMDrawingDataStore* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[MMDrawingDataStore alloc] init];
    });
    return instance;
}

#pragma mark - Public Methods

- (NSPersistentContainer *)persistentContainer {
    if (!_persistentContainer) {
        _persistentContainer = [NSPersistentContainer persistentContainerWithName:@"DrawingDataModel"];
        __block BOOL storesWereLoaded = NO;
        [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *psd, NSError *error) {
            if (error) {
                NSLog(@"loadPersistentStoresWithCompletionHandler: persistent container could not load store %@. Error: %@",
                      psd, error.debugDescription);
            } else {
                storesWereLoaded = YES;
            }
        }];
        if (storesWereLoaded) {
            NSLog(@"Stores were loaded");
        } else {
            NSLog(@"Stores were not loaded");
        }
    }
    return _persistentContainer;
}

@end
