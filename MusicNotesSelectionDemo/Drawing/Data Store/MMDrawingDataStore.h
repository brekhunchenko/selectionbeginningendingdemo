//
//  MMDrawingDataStore.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MMDrawingDataStore : NSObject

+ (instancetype)sharedInstance;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@end
