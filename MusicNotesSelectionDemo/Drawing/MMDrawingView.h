//
//  MMDrawingView.h
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDCurve+CoreDataClass.h"

@protocol MMDrawingViewDelegate;

@interface MMDrawingView : UIView

- (void)clear;

@property (nonatomic, weak) id<MMDrawingViewDelegate> delegate;

@property (nonatomic, strong) CDCurve* curve;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign) CGFloat lineWidth;

@end

@protocol MMDrawingViewDelegate <NSObject>

- (void)drawingView:(MMDrawingView *)view didFinishDrawingCurveWithPath:(UIBezierPath *)path;

@end
