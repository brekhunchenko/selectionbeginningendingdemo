//
//  MMDrawingView.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/27/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "MMDrawingView.h"

static const CGFloat kPointMinDistance = 5.0f;
static const CGFloat kPointMinDistanceSquared = kPointMinDistance * kPointMinDistance;

@interface MMDrawingView()

@property (nonatomic,assign) CGPoint currentPoint;
@property (nonatomic,assign) CGPoint previousPoint;
@property (nonatomic,assign) CGPoint previousPreviousPoint;

@end

@implementation MMDrawingView {
@private
    CGMutablePathRef _path;
}

#pragma mark - Initializers

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        _path = CGPathCreateMutable();
        _lineWidth = 5.0f;
        _lineColor = [UIColor redColor];
    }
    
    return self;
}

#pragma mark - Overridden

- (void)drawRect:(CGRect)rect {
    [self.backgroundColor set];
    UIRectFill(rect);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddPath(context, _path);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetStrokeColorWithColor(context, self.lineColor.CGColor);
    CGContextStrokePath(context);
}

#pragma mark - Custom Setters & Getters

- (void)setCurve:(CDCurve *)curve {
    _curve = curve;
    
    if (_curve != nil) {
        UIBezierPath* bezierPath = (UIBezierPath*)_curve.path;
        _path = CGPathCreateMutableCopy(bezierPath.CGPath);
        _lineColor = (UIColor *)curve.color;
        _lineWidth = curve.lineWidth;
    } else {
        _lineWidth = 5.0f;
        _lineColor = [UIColor redColor];
        _path = CGPathCreateMutable();
    }
    
    [self setNeedsDisplay];
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    self.previousPoint = [touch previousLocationInView:self];
    self.previousPreviousPoint = [touch previousLocationInView:self];
    self.currentPoint = [touch locationInView:self];
    
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    CGPoint point = [touch locationInView:self];
    
    CGFloat dx = point.x - self.currentPoint.x;
    CGFloat dy = point.y - self.currentPoint.y;
    if ((dx * dx + dy * dy) < kPointMinDistanceSquared) {
        return;
    }
    
    self.previousPreviousPoint = self.previousPoint;
    self.previousPoint = [touch previousLocationInView:self];
    self.currentPoint = [touch locationInView:self];
    
    CGPoint mid1 = midPoint(self.previousPoint, self.previousPreviousPoint);
    CGPoint mid2 = midPoint(self.currentPoint, self.previousPoint);
    CGMutablePathRef subpath = CGPathCreateMutable();
    CGPathMoveToPoint(subpath, NULL, mid1.x, mid1.y);
    CGPathAddQuadCurveToPoint(subpath, NULL,
                              self.previousPoint.x, self.previousPoint.y,
                              mid2.x, mid2.y);
    CGRect bounds = CGPathGetBoundingBox(subpath);
    CGRect drawBox = CGRectInset(bounds, -2.0 * self.lineWidth, -2.0 * self.lineWidth);
    CGPathAddPath(_path, NULL, subpath);
    CGPathRelease(subpath);
    
    [self setNeedsDisplayInRect:drawBox];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    [self _finishDrawingCurve];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    [self _finishDrawingCurve];
}

#pragma mark - Private Methods

- (void)_finishDrawingCurve {
    UIBezierPath* bezierPath = [[UIBezierPath alloc] init];
    bezierPath.CGPath = _path;
    if ([self.delegate respondsToSelector:@selector(drawingView:didFinishDrawingCurveWithPath:)]) {
        [self.delegate drawingView:self didFinishDrawingCurveWithPath:bezierPath];
    }
}

#pragma mark - Public Methods

- (void)clear {
    CGMutablePathRef oldPath = _path;
    CFRelease(oldPath);
    _path = CGPathCreateMutable();
    [self setNeedsDisplay];
    
    UIBezierPath* bezierPath = [[UIBezierPath alloc] init];
    bezierPath.CGPath = _path;
    if ([self.delegate respondsToSelector:@selector(drawingView:didFinishDrawingCurveWithPath:)]) {
        [self.delegate drawingView:self didFinishDrawingCurveWithPath:bezierPath];
    }
}

#pragma mark - Helpers function

CGPoint midPoint(CGPoint p1, CGPoint p2) {
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}

#pragma mark -

- (void)dealloc {
    CGPathRelease(_path);
}

@end
