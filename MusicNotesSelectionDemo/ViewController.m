//
//  ViewController.m
//  MusicNotesSelectionDemo
//
//  Created by Yaroslav Brekhunchenko on 3/2/18.
//  Copyright © 2018 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "MMSelectionLayer.h"
#import "MMLongPressGestureRecognizer.h"
#import "MMMagnifyingView.h"
#import "MMDrawingView.h"
#import "CDCurve+CoreDataExtensions.h"
#import "MMDrawingDataStore.h"

@interface ViewController () < UIActionSheetDelegate, MMLongPressGestureRecognizerDelegate, MMDrawingViewDelegate, UITextFieldDelegate >

@property (weak, nonatomic) IBOutlet MMMagnifyingView *musicSheetContainerView;
@property (weak, nonatomic) IBOutlet MMDrawingView *drawingView;

@property (nonatomic, strong) MMSelectionLayer* beginningSelectionLayer;
@property (nonatomic, strong) MMSelectionLayer* endingSelectionLayer;

@property (nonatomic, strong) MMLongPressGestureRecognizer* longPressGesture;
@property (nonatomic, strong) MMSelectionLayer* movementSelectionLayer;
@property (nonatomic, assign) CGPoint previousSelectionLayerLocation;

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pageTextField;

@end

@implementation ViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupSelectionLayers];
    [self _setupLongPressGesture];
    [self _setupDrawingView];
}

#pragma mark - Setup

- (void)_setupSelectionLayers {
    _beginningSelectionLayer = [[MMSelectionLayer alloc] init];
    [_musicSheetContainerView.layer insertSublayer:_beginningSelectionLayer atIndex:1];
    
    _endingSelectionLayer = [[MMSelectionLayer alloc] init];
    [_musicSheetContainerView.layer insertSublayer:_endingSelectionLayer atIndex:2];
    
    UIColor* color = [[UIColor greenColor] colorWithAlphaComponent:0.4];
    [_beginningSelectionLayer highlightRect:CGRectMake(123.0, 178, 8.0, 30.0) selectionType:MMSelectionLayerTypeRectangle color:color isBeginning:YES];
    [_endingSelectionLayer highlightRect:CGRectMake(270.0, 178.0, 8.0, 30.0) selectionType:MMSelectionLayerTypeRectangle color:color isBeginning:NO];
}

- (void)_setupLongPressGesture {
    _longPressGesture = [[MMLongPressGestureRecognizer alloc] initWithTarget:self action:@selector(_longPressGestureCatched:)];
    _longPressGesture.delegate = self;
    _longPressGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:_longPressGesture];
}

- (void)_setupDrawingView {
    _drawingView.delegate = self;
    
    NSString* username = self.usernameTextField.text;
    NSString* page = self.pageTextField.text;
    [self _updateDrawingForUsername:username page:page];
}

#pragma mark - Gestures

- (void)_longPressGestureCatched:(MMLongPressGestureRecognizer *)longPressGesture {
    CGPoint locationInView = [longPressGesture locationInView:self.view];
    
    if (longPressGesture.state == UIGestureRecognizerStateBegan) {
        if (CGRectContainsPoint(CGRectInset(_beginningSelectionLayer.rect, -20.0, -20.0), locationInView)) {
            _movementSelectionLayer = _beginningSelectionLayer;
        } else if (CGRectContainsPoint(CGRectInset(_endingSelectionLayer.rect, -20.0, -20.0), locationInView)) {
            _movementSelectionLayer = _endingSelectionLayer;
        }
        
        CGRect frame = _movementSelectionLayer.rect;
        frame.origin.x = locationInView.x - frame.size.width/2.0f;
        frame.origin.y = locationInView.y - frame.size.height/2.0f;
        [_movementSelectionLayer highlightRect:frame
                                 selectionType:_movementSelectionLayer.selectionType
                                         color:_movementSelectionLayer.color
                                   isBeginning:_movementSelectionLayer.isBeginning];
        _previousSelectionLayerLocation = locationInView;
    } else if (longPressGesture.state == UIGestureRecognizerStateChanged) {
        if (_movementSelectionLayer == nil) {
            return;
        }
        
        CGPoint movement = CGPointMake(locationInView.x - _previousSelectionLayerLocation.x, locationInView.y - _previousSelectionLayerLocation.y);
        CGRect previousFrame = _movementSelectionLayer.rect;
        CGRect updatedFrame = CGRectMake(previousFrame.origin.x + movement.x, previousFrame.origin.y + movement.y, previousFrame.size.width, previousFrame.size.height);
        [_movementSelectionLayer highlightRect:updatedFrame
                                 selectionType:_movementSelectionLayer.selectionType
                                         color:_movementSelectionLayer.color
                                   isBeginning:_movementSelectionLayer.isBeginning];

        _previousSelectionLayerLocation = locationInView;
    } else if (longPressGesture.state == UIGestureRecognizerStateEnded
               || longPressGesture.state == UIGestureRecognizerStateFailed
               || longPressGesture.state == UIGestureRecognizerStateCancelled) {
        _movementSelectionLayer = nil;
    }
}

#pragma mark - MMLongPressGestureRecognizerDelegate

- (void)longPressGestureRecognizer:(MMLongPressGestureRecognizer *)gestureRecognizer didMakePauseAtLocation:(CGPoint)location {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
//    [self _addTestingViewAtLocation:location];
}

#pragma mark - MMDrawingViewDelegate

- (void)drawingView:(MMDrawingView *)view didFinishDrawingCurveWithPath:(UIBezierPath *)path {
    NSString* username = self.usernameTextField.text;
    NSString* page = self.pageTextField.text;

    CDCurve* curve = [CDCurve findOrCreateCurveForUsername:username page:page arrangementID:@"test_arrangementID" scoreEdition:@"test_scoreEdition"];
    curve.path = path;
    curve.lineWidth = view.lineWidth;
    curve.color = view.lineColor;
    curve.username = self.usernameTextField.text;
    curve.page = self.pageTextField.text;
    [[MMDrawingDataStore sharedInstance].persistentContainer.viewContext save:nil];
}

- (void)_updateDrawingForUsername:(NSString *)username page:(NSString *)page {
    if (username.length == 0 || page.length == 0) {
        NSLog(@"Error. Empty page or username.");
    }
    
    CDCurve* curve = [CDCurve findCurveForUsername:username page:page arrangementID:@"test_arrangementID" scoreEdition:@"test_scoreEdition"];
    self.drawingView.curve = curve;
}

#pragma mark - Actions

- (IBAction)clearButtonAction:(id)sender {
    [self.drawingView clear];
}

#pragma mark - TESTING

- (void)_addTestingViewAtLocation:(CGPoint)location {
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
    view.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5f];
    view.center = location;
    [self.view addSubview:view];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    
    NSString* username = self.usernameTextField.text;
    NSString* page = self.pageTextField.text;
    [self _updateDrawingForUsername:username page:page];
    
    return YES;
}

@end
